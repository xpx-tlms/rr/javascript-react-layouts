# React Layouts
A repo that contains basic web layouts for React web apps.

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run the app: `npm start`

# Contents (Branches)
![](./docs/screen-shots.png)
- main
- [flex-101](./docs/flex-101.png)
- [flex-layout](./docs/flex-layout.png)
- [flex-layout-sidebar](./docs/flex-layout-sidebar.png)
- [grid](./docs/grid.png)
- grid-2
- [spotify](./docs/spotify.png)
- [spotify-template](./docs/spotify-template.png)
- [square-space](./docs/square-space.png)
- [holy-grail](./docs/holy-grail.png)
- css-positioning

# Flexbox Container Props
Default direction is left-to-right (row), this is the main axis, the cross axis is 90 degrees from th main axis.

- `display`: {flex|inline-flex} Inline shrink wraps flexbox items
- `flex-direction`: {column|row} Row is the default
- `flex-wrap`: Used to specify if the flex container should wrap flex items
- `flex-flow`: Shorthand for flex-direction and flex-wrap
- `justify-content`: {first baseline|center|space-between} along the main axis
- `align-items`: Aligns the flexbox items across the CROSS axis
- `align-content`: Applies alignment accross the CROSS axis for items that wrap multiple lines (does nothing for single line flex containers) stretch is default
- `gap`: Space between flexbox items

# Flexbox Item Props
- `order`: The order in which to render the flex item
- `flex-grow`: How much an item will GROW relative to the rest of the flexible items inside the same container
- `flex-shrink`: How much an item will SHRINK relative to the rest of the flexible items inside the same container
- `flex-basis`: The initial length of a flexible item
- `flex`: Shorthand for: `flex-grow` `flex-shrink` `flex-basis`
- `align-self`: Individually change [alignment](https://getbootstrap.com/docs/5.1/utilities/flex/#align-self) for flexbox items on the cross axis, including stretch

# Box Model: Margin, Border, Padding
- Margin is on the outside of the div (or element)
- Border is the actual border of the div
- Padding is on the inside of the div

# CSS Selectors
These selectors are used to target HTML elements:
- `body {}`: Simple 
- `#menu {}`: ID 
- `.bookTitle {}`: Class
- `div p {}`: Descendant (match any level)
- `div > p {}`: Child (match specifically) 
- `img[alt=spacer] {}`: Attribute
- `a:visited {}`: Psuedo
- `* {}`: All

# HTML Semantic Elements
- `<header>`
- `<nav>`
- `<main>`
- `<footer>`
- `<aside>` (sidebar?)

# Position
- `Default`: is static (vertical stacking)
- `Relative`: With respect to the previous element in the document flow
- `Absolute`: Takes element out of the document flow, and responds to scrolling
- `Fixed`: Takes element out of the document flow, and does NOT respond to scroll
![](./docs/css-position.png)

# Block vs Inline
- Inline elements do not render on a new line
- Block elements stack

# Notes
- [Flexbox](https://youtu.be/3YW65K6LcIA)
- [Common CSS Properties](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference)
- [Grid by Example](javascript-react-layout-assignments)
- [CSS Tricks Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [CSS Tricks Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
